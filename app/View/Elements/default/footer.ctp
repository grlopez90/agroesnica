<footer class="Footer">
	<iframe class="Footer-maps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62419.362448918175!2d-86.17735060776539!3d12.129419776506296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f73f90d312d2ca3%3A0x378308d197d124fd!2sAGROESNICA!5e0!3m2!1sen!2sni!4v1490643831119" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
	<div class="f-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-2 Footer-contact">
					<h4 class="Footer-title">Contacto</h4>
					<p>
						Tel: +1 (786) 738-6071 <br>
						(505) 2276-8260 / 68 <br>
						Fax: (505) 2276-8269 <br>
					</p>
					<p>
						info@agroesnica.com
					</p>
					<p>
						Finca San Jerónimo <br>
						Km 15.5 Carretera Vieja <br> 
						a Tipitapa. De la entrada <br>
						a Corrales Verdes 6km al sur. <br>
						Managua, Nicaragua
					</p>
				</div>
				<div class="col-md-4 col-md-offset-2">
					<h4 class="Footer-title">
						Queremos saber más de usted.
					</h4>
					<form action="">
						<div class="row">
							<div class="col-md-6">
								<input type="text" placeholder="Nombre" name="Nombre" class="form-control">
							</div>
							<div class="col-md-6">
								<input type="text" placeholder="Apellido" name="Apellido" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<input type="text" placeholder="Teléfono" name="telefono" class="form-control">
							</div>
							<div class="col-md-6">
								<input type="text" placeholder="Correo Electronico" name="email" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<textarea name="message" id="" cols="30" rows="5" class="form-control" placeholder="Mensaje"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="submit" class="btn btn-agro pull-right" value="Enviar">
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-3 col-md-offset-1">
					<h4 class="Footer-title">
						Galería de Fotos.
					</h4>
					<ul class="Galery-list">
						<li class="Galery-item">
						</li>
						<li class="Galery-item">
						</li>
						<li class="Galery-item">
						</li>
						<li class="Galery-item">
						</li>
						<li class="Galery-item">
						</li>
						<li class="Galery-item">
						</li>
					</ul>
					<?= $this->Html->image('agroesNica.png',['width'=>'100%']); ?>
				</div>	
			</div>
		</div>
	</div>
	<div class="ViewMaps">
		<div class="container">
			<a href="" id="ViewMaps" class="">
				<?= $this->Html->image('viewmaps.png');?>
			</a>
		</div>
	</div>
</footer>
<div class="Footer-bottom">
	<span>
		El nombre Agroesnica y su logotipo son marcas registradas. El contenido de este sitio es propiedad de Agroesnica,todos los derechos reservados. Sitio diseñado por <a href="http://www.creativocorp.com">www.CreativoCorp.com</a>
	</span>
</div>