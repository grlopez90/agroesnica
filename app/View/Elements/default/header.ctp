<div class="HeaderMain">
	<div class="container">
		<div class="row">
			<div class="col-md-3 Headerlogo">
                <?= $this->Html->image('agroesNica.png',['class'=>'img-responsive', 'alt'=>'Radio Nicaragua', 'url'=>$this->Html->url('/',true)]); ?>
            </div>
            <div class="col-md-9">
                <div class="Header-menuContainer">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"></a>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="class="<?= ($this->request['controller'] == 'homes')?'active':'' ?>"><a href="<?= $this->Html->url('/'); ?>">Inicio</a></li>
                                    <li class="<?= ($this->request['controller'] == 'about')?'active':'' ?>"><a href="<?= $this->Html->url('/about/'); ?>">¿Quienes Somos?</a></li>
                                    <li class="<?= ($this->request['controller'] == 'exports')?'active':'' ?>"><a href="<?= $this->Html->url('/exports/index/'); ?>">Nuestras Exportaciones</a></li>
                                    <li class="<?= ($this->request['controller'] == 'certifications')?'active':'' ?>"><a href="<?= $this->Html->url('/certifications'); ?>">Certificaciones</a></li>
                                    <li class="<?= ($this->request['controller'] == 'news')?'active':'' ?>"><a href="<?= $this->Html->url('/news'); ?>">Noticias</a></li>
                                    <li class="<?= ($this->request['controller'] == 'albums')?'active':'' ?>"><a href="<?= $this->Html->url('/albums'); ?>">Galería de Imagenes</a></li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                </div>
            </div>
		</div>
	</div>
</div>
<aside class="socials">
    <ul>
        <li>
            <a href="">
                <?= $this->Html->image('face.png'); ?>
            </a>
        </li>
        <li>
            <a href="">
                <?= $this->Html->image('you.png'); ?>
            </a>
        </li>
    </ul>
</aside>        