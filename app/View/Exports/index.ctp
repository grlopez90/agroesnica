<div class="ExportsMainSection">
	<div class="ems-wrapper">
		<h2 class="title ems-title">
			Nuestros Productos
		</h2>
		<h3 class="title">
			Comprometidos por excelencia garantizamos un producto de calidad desde el cultivo hasta el destino final.
		</h3>
	</div>
</div>

<div class="Productos">
	<div class="bodeguero">
		<?= $this->Html->image('bodeguero.png',['class'=>'img-responsive']); ?>
	</div>
	<div class="Productos-charact container">
		<div class="row">
			<div class="col-md-4 col-md-offset-1">
				<h3 class="title">Okra Fresca</h3>
				<p class="text">
					De origen africano, okra o gumbo pertenece a la familia de las Malváceas. Es reconocido por su fresco color verde, sabor y nutrición.  Contribuye a mejorar la dieta alimenticia, debido a sus diferentes nutrientes tales como proteínas, carbohidratos, fibra, calcio, fósforo, hierro, vitaminas A, B y C. La característica gelatinosa que posee, le brinda propiedades que previenen enfermedades, entre ellas el asma, afecciones gástricas, estreñimiento, colesterol, arteriosclerosis y cataratas.
				</p>
			</div>
			<div class="col-md-4 col-md-offset-2">
				<h3 class="title">Características</h3>
				<p class="text">
					<ul>
						<li>Calidad: Grado A – E.E.U.U No. 1</li>
						<li>Variedad: Clemson Spineless</li>
						<li>Transporte: Aéreo</li>
					</ul>
				</p>
			</div>
		</div>
	</div>
	<div class="AdvantageDown">
		<h3 class="title">
			Tenemos una producción lineal durante todo el año. 
		</h3>
		<p>
			Embarcamos diariamente aéreo lo que asegura el cumplimiento de entrega y la calidad de nuestro producto. 
		</p>
		<h3 class="title">
			Tiempo en tránsito de 24-48 horas 
		</h3>
		<p>
			después de cosecha (según destino)
		</p>
	</div>
</div>

<div class="Boxes">
	<div class="container">
		<div class="row">
			<div class="col-md-4 box">
				<span class="Boxes-title">CLEMSON SPINELESS</span> <br>
				<span class="Boxes-subtitle">(OKRA AMERICANA)</span>
				<?= $this->Html->image('plantas1.png',['class'=>'img-responsive']); ?>
			</div>
			<div class="col-md-4 a-wrapper">
				<span class="Boxes-title Anaquel">
					Vida de <br>
					Anaquel <br>
					10-15 días
				</span>
			</div>
			<div class="col-md-4 box">
				<span class="Boxes-title">OKRA HINDU</span> <br>
				<span class="Boxes-subtitle">&nbsp;</span>
				<?= $this->Html->image('export-okra-02.jpg',['class'=>'img-responsive']); ?>
			</div>
		</div>
	</div>
</div>
<div class="BoxMain">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="title">Nuestras Cajas</h3>
				<p class="text">
					Tradicionalmente okra fresca se vendía en cajas de madera, las cuales no garantizaban la inocuidad del producto, con el cual estamos altamente comprometidos.    Basado en lo anterior fuimos los pioneros en implementar una caja de Kartonplast, que garantiza al consumidor final, inocuidad, presentación y mayor frescura del producto
				</p>
			</div>
		</div>
	</div>
</div>
<div class="BoxHistory">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="title">
					Historia
				</h3>
				<p>
					En nuestro continuo proceso de investigación de nuevos productos decidimos enfocarnos en un nicho de mercado en donde pudiéramos proveer productos de muy alta calidad con una consistencia y permanencia en el mercado durante todos los meses del año. 
				</p>
				<p>
					Fue en el año 2001 cuando iniciamos con un pequeño volumen hacia mercado americano, encontrándonos con el reto de  llenar las mayores expectativas del consumidor final.     Luego de 5 años de constante implementación de nuevos procesos de manejo integral de campo y manejo post-cosecha,  conseguimos abrir puertas en el mercado Canadiense y Europeo..

				</p>
			</div>
			<div class="col-md-6">
				<h3 class="title">&nbsp</h3>
				<p>
					Conseguimos consolidarnos en los mercados como ¨la Okra de la Caja Blanca¨ ó  ¨la Okra de Nicaragua¨ y es nuestro cliente quien agregó valor a través de los años basado en calidad, consistencia y alta eficiencia. 
				</p>
				<p>
					Actualmente hemos re-lanzado una nueva presentación de empaque al mercado que representa:  
				</p>
				<p>
					<ul>
						<li>La frescura de nuestro producto</li>
						<li>La vitalidad que nuestro personal </li>
						<li>y la seguridad que nos caracteriza</li>
					</ul>
				</p>
				<p class="green">
					Tres puntos que significan tanto <br> para AGROESNICA. 
				</p>
			</div>
		</div>
	</div>
</div>

<div class="ExportsCircle">
	<div class="container">
		<ul class="ec-wrapper row">
			<li class="ExportsCircle-item col-md-3 col-sm-6">
				<?= $this->Html->image('exports-tipos-01.jpg', ['width'=>'img-responsive']); ?>
			</li>
			<li class="ExportsCircle-item col-md-3 col-sm-6">
				<?= $this->Html->image('exports-tipos-02.jpg', ['width'=>'img-responsive']); ?>
			</li>
			<li class="ExportsCircle-item col-md-3 col-sm-6">
				<?= $this->Html->image('exports-tipos-03.jpg', ['width'=>'img-responsive']); ?>
			</li>
			<li class="ExportsCircle-item col-md-3 col-sm-6">
				<?= $this->Html->image('exports-tipos-04.jpg', ['width'=>'img-responsive']); ?>
			</li>
		</ul>
	</div>
</div>