<div>
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="btn-group" role="group" aria-label="...">
				<?php echo $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;Listar Noticias'), array('action' => 'index'),array('class' => 'btn btn-primary', 'escape'=>false)); ?>
				<?php echo $this->Form->postLink(__('<i class="fa fa-times"></i>&nbsp;&nbsp;Eliminar Noticia'), array('action' => 'delete', $this->Form->value('News.id')), array('class' => 'btn btn-danger', 'escape'=>false,'confirm' => __('Seguro que desea eliminar la noticia: "%s"?', $this->Form->value('News.title')))); ?>
			</div>
		</div>
	</div>
	<div class="news form">
	<?php echo $this->Form->create('News',array('inputDefaults' => array('required' => false))); ?>
		<fieldset>
			<legend><?php echo __('Editar Noticia'); ?></legend>
			<div class="row">
				<div class="col-md-12">
					<?php echo $this->Form->label('Imagen'); ?>
					<div class="head_upload">
						<input id="file_upload" name="file_upload" type="file" multiple="true">
						<a class="upload_all btn btn-success" style="position: relative;" href="javascript:$('#file_upload').uploadifive('upload')" title="Subir Archivos">
							<span class="glyphicon glyphicon-open"></span>
						</a>
					</div>
					<div id="queue">
						<div class="print_images">
							<?php
							if (isset($this->request->data['Image'])) foreach($this->request->data['Image'] as $a) {


								$src = $a['seccion'].'/'.$a['id'].'.'.$a['extension'];

								if (file_exists(Configure::read('absolute_root').$src)) {

									echo $this->Html->tag(
										'div',
										$this->Html->image(
											'/files/'.$src  ,
											array(
												'class' => 'pic'
											)
										).
										$this->Html->link(
											'',
											'/upload/delete_imagen/'.$a['id'],
											array(
												'class' => 'delete drop icon_control',
												'escape' => false,
												'data' => $a['id']
											)
										),
										array(
											'class' => 'pic_wrapper',
											'style' => 'width:auto;',
											'id' => $a['id']
										)
									);
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php
				echo $this->Form->input('id');
				echo $this->Form->input('title', array('class'=>'form-control', 'div'=>array('class' => 'col-md-12'), 'label'=>'Título'));
				?>
			</div>
			<div class="row">
				<div class="text inputFloat col-md-2">
					<?php echo $this->Form->label('Estado', null, array('class' => 'labelStatus')); ?>
					<?php echo $this->Form->input('state',array('type'=>'checkbox', 'label'=>false, 'div'=>false, 'data-toggle' => 'toggle')); ?>
				</div>
			</div>
			<div class="row">
				<?php echo $this->Form->input('content', array('class'=>'form-control wysiwyg', 'div'=>array('class' => 'col-md-12'), 'label'=>'Contenido')); ?>
			</div>
		</fieldset>
		<?php echo $this->Form->end(array('label'=>__('Guardar'),'class'=>'btn btn-success')); ?>
	</div>
</div>

<script type="text/javascript">
	<?php
	$timestamp = time();
	$seccion = base64_encode('news');
	$url = $this->Html->url('/upload/Upload_File/?seccion='.$seccion);
	$check = $this->Html->url('/upload/check_exists/?seccion='.$seccion);
	$img = @$this->request->data['News']['id'];
	?>
	$(function() {

		$('#file_upload').uploadifive({
			'auto'             : false,
			'checkScript'      : '<?php echo $check ?>',
			'formData'         : {
								   'timestamp' : '<?php echo $timestamp;?>',
								   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
			                     },
			'queueID'          : 'queue',
			'uploadScript'     : '<?php echo $url."&img=".$img; ?>',
			'onUploadComplete' : function(file, data) {

				$('.print_images').empty();

				$('.print_images').append(data);

			}
		});

		$('.drop').delete_img();
	});
</script>