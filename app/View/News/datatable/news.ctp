<?php
foreach ($dtResults as $result) {
    $actions = $this->Html->link(__('<i class="fa fa-fw fa-pencil"></i> Editar'), array('action' => 'edit', $result['News']['id']), array('class'=>'btn btn-success', 'escape'=>false));
    $actions .= ' '.$this->Form->postLink(__('<i class="fa fa-fw fa-times"></i> Eliminar'), array('action' => 'delete', $result['News']['id']), array('class'=>'btn btn-danger', 'escape'=>false), __('Seguro que desea eliminar la noticia: "%s"?', $result['News']['title']));
    if ($result['News']['state'] == 1)
    	$result['News']['state'] = 'Activo';
    else
    	$result['News']['state'] = 'Inactivo';
    $this->dtResponse['aaData'][] = [
        $result['News']['id'],
        $result['News']['title'],
        $result['News']['state'],
        $result['News']['created'],
        $actions,
    ];
}
?>
