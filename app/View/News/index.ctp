<div class="News">
	<div class="News-top">
		<div class="container">
			<div class="text">
				<h1>News & Articles</h1>
				<p>We constantly improve our products <br/>
				and services stay in sync with our <br/>
				content and our social media.</p>
				<div class="socialButtons">
					<?= $this->Html->image('YouTube.png'); ?>
					<?= $this->Html->image('Facebook.png'); ?>
					<?= $this->Html->image('Twitter.png'); ?>
					<?= $this->Html->image('mail.png'); ?>
				</div>
			</div>
			<div class="image">
				<?= $this->Html->image('news-tresPersonas.png'); ?>
			</div>
		</div>
	</div>

	<div class="News-list">
		<div class="container">
			<article>
				<div class="right">
					<div class="image">
						<?= $this->Html->image('/files/imageNews.jpg'); ?>
					</div>
				</div>
				<div class="left">
					<div class="header">
						<h2>Title of the most resent news or article</h2>
					</div>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
					</div>
					<?= $this->Html->link(
						'More info...', 
						array(
							'controller'=>'news',
							'action'=>'view',
							'title' => 'id-permalink'
						), 
						array('class'=>'button')
					); ?>
				</div>
			</article>
			<article>
				<div class="right">
					<div class="image">
						<?= $this->Html->image('/files/imageNews.jpg'); ?>
					</div>
				</div>
				<div class="left">
					<div class="header">
						<h2>Title of the most resent news or article</h2>
					</div>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
					</div>
					<?= $this->Html->link(
						'More info...', 
						array(
							'controller'=>'news',
							'action'=>'view',
							'title' => 'id-permalink'
						), 
						array('class'=>'button')
					); ?>
				</div>
			</article>
			<article>
				<div class="right">
					<div class="image">
						<?= $this->Html->image('/files/imageNews.jpg'); ?>
					</div>
				</div>
				<div class="left">
					<div class="header">
						<h2>Title of the most resent news or article</h2>
					</div>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
					</div>
					<?= $this->Html->link(
						'More info...', 
						array(
							'controller'=>'news',
							'action'=>'view',
							'title' => 'id-permalink'
						), 
						array('class'=>'button')
					); ?>
				</div>
			</article>
		</div>
	</div>
</div>