<div class="news index">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="btn-group" role="group" aria-label="...">
				<?php echo $this->Html->link('<i class="fa fa-plus"></i>&nbsp;&nbsp;Nueva Noticia', '/admin/news/add', array('class'=>'btn btn-primary', 'escape' => false)); ?>
			</div>
		</div>
	</div>
	<div class="table-responsive">
		<?php echo $this->DataTable->render('News') ?>
	</div>
</div>
