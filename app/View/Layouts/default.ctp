<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0">
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('default');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div class="Header">
		<?= $this->element('Default/header'); ?>
	</div>
	<div id="content">
		<?php echo $this->Flash->render(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
	<div class="Footer">
		<?= $this->element('Default/footer'); ?>
	</div>
	<?php echo $this->element('sql_dump'); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/65d2312afa.js"></script>
	<?= $this->Html->script('colorbox.min'); ?>
	<?= $this->Html->script('common'); ?>

	<script>
		$('#ViewMaps').click(function(event){
			event.preventDefault();
			$this = $(this);
			$maps = $('.Footer-maps');
			$footer = $('.f-wrapper');

			if ($maps.is(':visible')) {
				$maps.hide('slow/400/fast', function() {	
				});
				$footer.show('slow/400/fast', function() {	
				});
			} else {
				$maps.show('slow/400/fast', function() {	
				});
				$footer.hide('slow/400/fast', function() {	
				});
			}

		});
	</script>
</body>
</html>
