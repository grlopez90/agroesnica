<!DOCTYPE html>
<html>
  <head>
  	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('admin');
        echo $this->Html->css('//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css');

		echo $this->Html->script('jquery');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js');

		echo $this->Html->script('tinymce/tinymce.min.js');
        echo $this->Html->script('jquery.uploadifive.min');
        echo $this->Html->script('bootstrap-toggle.min');
        echo $this->Html->script('chosen.jquery.min');
        echo $this->Html->script('commonAdmin');

		echo $this->fetch('meta');
		echo $this->fetch('css');
        echo $this->fetch('dataTableSettings');
		echo $this->fetch('script');

	?>

</head>
<body>
    <header class="container-fluid">
        <div class="row">
            <div class="topAdmin">
                <?php echo $this->element('admin/main_menu'); ?>
            </div>
        </div>
    </header>
    <div class="container-fluid">
        <div id="wrapper_main">
            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo $title_for_layout; ?></div>
                <div class="panel-body">
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>
    </div>
	<?php //echo $this->Session->flash(); ?>
    <div class="loading">
        <embed src="<?php echo $this->Html->url('/img/loading.svg'); ?>" type=""></embed>
    </div>
    <script>
        $(document).ajaxStart(function() { $('.loading').show(); });
        $(document).ajaxComplete(function() { $('.loading').hide(); });
    </script>
</body>
</html>
