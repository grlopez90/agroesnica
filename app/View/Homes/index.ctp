<div class="MainSection">
	<div id="carousel-example-generic" class="carousel slide HomenNews-primary" data-ride="carousel">
	    <!-- Indicators -->
	    <ol class="carousel-indicators">
	        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
	        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
	        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	    </ol>
	    <!-- Wrapper for slides -->
	    <div class="carousel-inner" role="listbox">
	        <div class="item active">
	            <?= $this->Html->image('slide/slide1.jpg', ['class'=>'img-responsive']);  ?>
	        	<div class="container" style="position: relative; max-width: 1170px">
		            <?= $this->Html->image('slide/slide1-inner.png', ['class'=>'img-responsive img-innerSlide']);  ?>
		            <p class="text-innerSlide">
		            	DESDE NICARAGUA <br> PARA EL MUNDO... <br> <span class="best">LO MEJOR</span>
		            </p>
	            </div>
	            <div class="carousel-caption">
	                
	            </div>
	        </div>
	        <div class="item">
	            <?= $this->Html->image('slide/slide2.jpg', ['class'=>'img-responsive']);  ?>
	            <div class="container" style="position: relative; max-width: 1170px">
	            	<?= $this->Html->image('slide/slide2-inner.png', ['class'=>'img-responsive img-innerSlide']);  ?>
		            <p class="text-innerSlide">
		            	<span class="best">Compromiso</span> <br> 
		            	Tenemos un compromiso <br>
		            	con nuestra gente <br>
		            	y nuestros clientes
		            </p>
		            <div class="carousel-caption">
		                
		            </div>
	            </div>
	        </div>
	        <div class="item">
	            <?= $this->Html->image('slide/slide3.jpg', ['class'=>'img-responsive']);  ?>
	            <div class="container" style="position: relative; max-width: 1170px">
		            <?= $this->Html->image('slide/slide3-inner.png', ['class'=>'img-responsive img-innerSlide']);  ?>
		            <p class="text-innerSlide">
		            	Somos Una <br>
		            	Gran Familia <br>
		            </p>
		            <div class="carousel-caption">
		                
		            </div>
		        </div>
	        </div>
	    </div>
	    <!-- Controls -->
	    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	    </a>
	    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	    </a>
	</div>
</div>

<div class="aboutHome">
	<div class="container">
		<div class="row">
			<div class="col-md-6 aboutHome-info">
				<h2 class="title">
					Central America’s Leader in Okra Production
				</h2>
				<p class="text">
					Agro España Nicaragua (AGROESNICA) ubicada en Managua, Nicaragua, Centro América. Comprometidos con la excelencia, líderes en el mercado mundial en la producción de Okra Fresca. 
				</p>
				<a href="" class="btn btn-agro">
					Mas Información
				</a>
			</div>
			<div class="col-md-6 aboutHome-image">
				<?= $this->Html->image('abouthome.jpg',['class'=>'img-responsive']); ?>
			</div>
		</div>
	</div>
</div>

<div class="advantages">
	<div class="container">
		<figure class="advantagesLogo">
			<?= $this->Html->image('logobig.png',['class'=>'img-responsive']); ?>
		</figure>
		<div class="row">
			<div class="col-md-4">
				<div class="Advantage">
					<?= $this->Html->image('advantages/hands.png',['classs'=>'img-responsive']); ?>
					<h3>Trabajo en Equipo</h3>
					Con mas de 3,000 colaboradores
					dependemos de buenas prácticas
					para llevar a cabo las tareas del dia a dia
				</div>
			</div>
			<div class="col-md-4">
				<div class="Advantage">
					<?= $this->Html->image('advantages/hojas.png',['classs'=>'img-responsive']); ?>
					<h3>Mejores Cultivos</h3>
					Tenemos muchos años de
					mejoras continuas. Hemos alcanzado
					lograr cosechas mas saludables
					gracias al constate monitoreo
				</div>
			</div>
			<div class="col-md-4">
				<div class="Advantage">
					<?= $this->Html->image('advantages/robot.png',['classs'=>'img-responsive']); ?>
					<h3>Optimización Tecnológica</h3>
					Investigamos y desarrollamos los
					recursos de tecnología de forma
					nativa, para asegurar un mejor
					producto a nuestros consumidores
				</div>
			</div>
		</div>
	</div>
</div>

<div class="Exportaciones">
	<div class="container">
		<div class="Exportaciones-header">
			<!-- <h3 class="title">Nuestras Exportaciones</h3>
			<span class="subtitle">LA MAYORIA DE LAS EXPORTACIONES TIENEN DESTINOS EN EUROPA Y CANADÁ</span> -->
		</div>
	</div>
	<div class="Exportaciones-maps">
		
	</div>
</div>

<div class="Quality">
	<div class="container">
		<div class="Quality-header">
			<h3 class="title">
				Calidad Certificada
			</h3>
			<span class="subtitle">Nuestro proceso patentado garantiza la calidad de nuestros productos en cualquier destino del mundo</span>
		</div>
	</div>
	<div class="Q-wrapper">
		<ul class="Quality-list">
			<li class="Quality-item">
				<?= $this->Html->image('logos/bpa.png',['class'=>'img-responsive']); ?>
			</li>
			<li class="Quality-item">
				<?= $this->Html->image('logos/bpm.png',['class'=>'img-responsive']); ?>
			</li>
			<li class="Quality-item">
				<?= $this->Html->image('logos/global.png',['class'=>'img-responsive']); ?>
			</li>
			<li class="Quality-item">
				<?= $this->Html->image('logos/haccp.png',['class'=>'img-responsive']); ?>
			</li>
			<li class="Quality-item">
				<?= $this->Html->image('logos/primus.png',['class'=>'img-responsive']); ?>
			</li>
		</ul>
	</div>

	<div class="AboutFoot">
		<div class="container">
			<a href="" class="btn btn-agro">
				Acerca de Nosotros
			</a>
			<?= $this->Html->image('home-certif-logo.png', ['class'=>'img-responsive AboutFoot-logo']); ?>
		</div>
	</div>
</div>



