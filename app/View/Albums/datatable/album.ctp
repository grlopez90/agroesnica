<?php
foreach ($dtResults as $result) {
    $actions = $this->Html->link(__('<i class="fa fa-fw fa-pencil"></i> Editar'), array('action' => 'edit', $result['Album']['id']), array('class'=>'btn btn-success', 'escape'=>false));
    $actions .= ' '.$this->Form->postLink(__('<i class="fa fa-fw fa-times"></i> Eliminar'), array('action' => 'delete', $result['Album']['id']), array('class'=>'btn btn-danger', 'escape'=>false), __('Seguro que desea eliminar la galería: "%s"?', $result['Album']['title']));
    $actions .= ' '.$this->Html->link(__('<i class="fa fa-fw fa-picture-o"></i> Subir Fotos'), array('action' => 'view', $result['Album']['id']), array('class'=>'btn btn-info', 'escape'=>false));
    if ($result['Album']['state'] == 1)
    	$result['Album']['state'] = 'Activo';
    else
    	$result['Album']['state'] = 'Inactivo';
    $this->dtResponse['aaData'][] = [
        $result['Album']['id'],
        $result['Album']['title'],
        $result['Album']['state'],
        $result['Album']['created'],
        $actions,
    ];
}
?>
