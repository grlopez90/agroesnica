<div class="Albums">
	<div class="container">
		<header class="header col-md-12">
			<h1>Galerías</h1>
		</header>
		<div class="list">
			<?php foreach ($albums as $album): ?>
				<div class="col-md-4 col-xs-6 album">
					<figure>
						<?php if (!empty($album['Image'])): ?>
							<?= $this->Html->image('/files/albums/'.$album['Image'][0]['id'].'/'.$album['Image'][0]['extension']); ?>
						<?php endif ?>
					</figure>
					<h3><?= $album['Album']['title'] ?></h3>
					<?= $this->Html->link(
						'Ver Fotos', 
						'/albums/view/'.$album['Album']['id'].'/'.$album['Album']['permalink'],
						array('class'=>'button')
					); ?>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>