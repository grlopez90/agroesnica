<div>
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="btn-group" role="group" aria-label="...">
				<?php echo $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;Listar Galerías'), array('action' => 'index'),array('class' => 'btn btn-primary','escape'=>false)); ?>
			</div>
		</div>
	</div>
	<div class="news form">
	<?php echo $this->Form->create('Album',array('inputDefaults' => array('required' => false))); ?>
		<fieldset>
			<legend><?php echo __('Nueva Galería'); ?></legend>
			<div class="row">
				<?php
				echo $this->Form->input('title', array('class'=>'form-control', 'div'=>array('class' => 'col-md-10'), 'label'=>'Título')); ?>
				<div class="text inputFloat col-md-2">
					<?php echo $this->Form->label('Estado', null, array('class' => 'labelStatus')); ?>
					<?php echo $this->Form->input('state',array('type'=>'checkbox', 'label'=>false, 'div'=>false, 'data-toggle' => 'toggle')); ?>
				</div>
			</div>
			<div class="row">
				<?php echo $this->Form->input('content', array('class'=>'form-control wysiwyg', 'div'=>array('class' => 'col-md-12'), 'label'=>'Contenido')); ?>
			</div>
		</fieldset>
		<?php echo $this->Form->end(array('label'=>__('Guardar'),'class'=>'btn btn-success')); ?>
	</div>
</div>
