<div>
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="btn-group" role="group" aria-label="...">
				<?php echo $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;Listar Galerías'), array('action' => 'index'),array('class' => 'btn btn-primary','escape'=>false)); ?>
				<?php echo $this->Html->link(__('<i class="fa fa-pencil"></i>&nbsp;&nbsp;Editar Galería'), array('action' => 'edit', $album['Album']['id']), array('class'=>'btn btn-info','escape'=>false)); ?>
				<?php echo $this->Form->postLink(__('<i class="fa fa-times"></i>&nbsp;&nbsp;Eliminar Galería'), array('action' => 'delete', $album['Album']['id']), array('class'=>'btn btn-danger','escape'=>false), __('Seguro que desea eliminar la galería: "%s"?', $album['Album']['title'])); ?>
			</div>
		</div>
	</div>
	<div class="albums view">
		<fieldset>
			<legend><?php echo __('Subir Fotos a la Galería'); ?></legend>
			<div id="pics">
			<?php
			if (isset($album['Image'])) foreach($album['Image'] as $a) {


				$src = $a['seccion'].'/thumbs/'.$a['id'].'.'.$a['extension'];

				/*if (file_exists(Configure::read('absolute_root').$src)) {*/

					echo $this->Html->tag(
						'div',
						$this->Html->image(
							'/files/'.$src  ,
							array(
								'class' => 'pic'
							)
						).
						$this->Html->link(
							$this->Html->image(
								'admin/delete.png',
								array(
									'style' => 'position:absolute;right:10px;top:10px',
									'class' => 'icon_control'
								)
							),
							'/upload/delete_imagen/'.$a['id'],
							array(
								'class' => 'drop',
								'escape' => false,
								'data' => $a['id']
							)
						),
						array(
							'class' => 'pic_wrapper',
							'style' => 'width:161px;text-align:center;',
							'id' => $a['id']
						)
					);
				/*}*/
			}
			?>
			</div>
			<div class="head_upload">
				<input id="file_upload" name="file_upload" type="file" multiple="true">
				<a class="upload_all btn btn-success" style="position: relative;" href="javascript:$('#file_upload').uploadifive('upload')" title="Subir Archivos">
					<span class="glyphicon glyphicon-open"></span>
				</a>
			</div>
			<div id="queue">
				<div class="print_images"></div>
			</div>
		</fieldset>
	</div>
</div>

<script type="text/javascript">
	<?php
	$timestamp = time();
	$seccion = base64_encode('albums');
	$url = $this->Html->url('/upload/Upload_File/?seccion='.$seccion);
	$check = $this->Html->url('/upload/check_exists/?seccion='.$seccion);
	$img = @$album['Album']['id'];
	?>
	$(function() {
		$('#file_upload').uploadifive({
			'auto'             : false,
			'checkScript'      : '<?php echo $check ?>',
			'formData'         : {
								   'timestamp' : '<?php echo $timestamp;?>',
								   'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
			                     },
			'queueID'          : 'queue',
			'uploadScript'     : '<?php echo $url."&img=".$img."&multi=true" ?>',
			'onUploadComplete' : function(file, data) {
				//$('.print_images').empty();
				$('.print_images').append(data);
				$('#pics').append(data);

			}
		});
		$('.drop').delete_img();
	});
</script>
