<section class="Album">
	<div class="container">
		<div class="col-md-12">
			<header class="Album-header">
				<h1><?= $album['Album']['title'] ?></h1>
				<span class="date">Subido 18/10/2016</span>
			</header>
			<div class="Album-content">
				<p><?= $album['Album']['content'] ?>.</p>
			</div>
		</div>
		<div class="photos">
			<?php foreach ($album['Image'] as $image): ?>
				<div class="photo col-md-3 col-sm-4 col-xs-6">
					<?= $this->Html->link( 
		      			$this->Html->image(
		      				'/files/albums/'.$image['id'].'.'.$image['extension'],
		      				array(
		      					'alt'=>'titulo'
		      				)
			      		),
						$this->Html->url('/files/albums/'.$image['id'].'.'.$image['extension'],true), 
						array(
							'escape' => false,
							'class' => 'group_photo cboxElement'
						)
			    	); ?>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</section>