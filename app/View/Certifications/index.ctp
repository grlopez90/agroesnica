<div class="AwardMain">
	<div class="am-wrapper">
		<div class="pull-right">
			<h3 class="title">
				Certificaciones
			</h3>
			<p class="AwardMain-content">
				En Nicaragua, las certificaciones en cultivos <br>
				agrícolas y el área de Inocuidad Agroalimentaria <br>
				son regidas por el Instituto de Protección y <br>
				Sanidad Agropecuaria (IPSA)
			</p>
		</div>
	</div>
	<div class="am-imageBg"></div>
	<?= $this->Html->image('certif-okra-top.png',['class'=>'floatSeed']); ?>
	<div class="clearfix"></div>
</div>

<div class="Control">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="title">
					Control y Calidad
				</h3>
				<p>
					Brindar productos de calidad es nuestra máxima prioridad y debido a lo mismo controles basados en los más estrictos estándares internacionales se realizan bajo la supervisión de ejecutivos especializados. Programas como los de Buenas Prácticas Agrícolas y Buenas Prácticas de Manufactura nos permiten garantizar alimentos inocuos a nuestros clientes por medio de la creación de guías y procesos más organizados que permiten tomar todas las normas preventivas y correctivas desde la siembra hasta que llega a su destino final.
				</p>
				<p>
					Nuestra experiencia nos ha permitido cumplir con las normas internacionales,  basadas en la protección de  la salud del consumidor, exportar a mercados más exigentes,  prevenir y minimizar el rechazo de los productos. 
				</p>
			</div>
			<div class="col-md-6">
				<?= $this->Html->image('lab.png',['class'=>'img-responsive']); ?>
			</div>
		</div>
	</div>
</div>

<div class="Awards">
	<h3 class="title">
		AGROESNICA ha obtenido las siguientes certificaciones:
	</h3>

	<ul class="Award-list">
		<li class="Award-item">Buenas Prácticas Agrícolas, BPA </li>
		<li class="Award-item">GLOBAL GAP</li>
		<li class="Award-item">Buenas Prácticas de Manufactura, BPM</li>
		<li class="Award-item">Análisis de  Riesgos y Puntos Críticos de Control (HACCP)</li>
	</ul>
	<div class="container">
		<div class="Award-quality Q-wrapper">
			<ul class="Quality-list">
				<li class="Quality-item">
					<?= $this->Html->image('logos/bpa.png',['class'=>'img-responsive']); ?>
				</li>
				<li class="Quality-item">
					<?= $this->Html->image('logos/bpm.png',['class'=>'img-responsive']); ?>
				</li>
				<li class="Quality-item">
					<?= $this->Html->image('logos/global.png',['class'=>'img-responsive']); ?>
				</li>
				<li class="Quality-item">
					<?= $this->Html->image('logos/haccp.png',['class'=>'img-responsive']); ?>
				</li>
				<li class="Quality-item">
					<?= $this->Html->image('logos/primus.png',['class'=>'img-responsive']); ?>
				</li>
			</ul>
			<div class="Award-qualityInfo">
				<p class="text">
					Además se ha obtenido la certificación a nivel Internacional para la totalidad de la finca y nuestra nave de empaque con PRIMUS.LABS.COM en estándares de calidad que garantizan la inocuidad de nuestros productos tanto en campo como en proceso.
				</p>
			</div>
		</div>
	</div>
</div>

<div class="Membresias">
	<h3 class="title">
		Membresias:
	</h3>

	<ul class="Award-list">
		<li class="Award-item">PMA </li>
		<li class="Award-item">BlueBook (#172-937)</li>
		<li class="Award-item">APEN</li>
	</ul>
	<ul class="Quality-list">
		<li class="Quality-item">
			<?= $this->Html->image('logos2/pma.png',['class'=>'img-responsive']); ?>
		</li>
		<li class="Quality-item">
			<?= $this->Html->image('logos2/ist.png',['class'=>'img-responsive']); ?>
		</li>
		<li class="Quality-item">
			<?= $this->Html->image('logos2/apen.png',['class'=>'img-responsive']); ?>
		</li>
	</ul>
</div>