<div class="About">
	<section class="About-history">
		<div class="container">
			<div class="row">
				<div class="left col-md-6">
					<h1>Nuestra Historia</h1>
					<h2>¿Quiénes Somos?</h2>
					<div class="text">
						<p>Agro España Nicaragua S.A (AgroEsNica) es la más grande productora, comercializadora y exportadora de Okra y otros productos no tradicionales en Nicaragua hacia el mundo.</p>
						<p>De capital Español - Nicaragüense nos hemos reconocido desde hace más de 15 años en los más importantes mercados internacionales de Estados Unidos, Canada y Europa, caracterizándonos por utilizar las técnicas de cultivo más avanzadas, respetuosas al medio ambiente , bajo las normas de calidad más exigentes y soportados por un capital humano invaluable.</p>
					</div>
				</div>
				<div class="right col-md-6">
					<figure class="image">
						<?= $this->Html->image('/files/aboutTop.jpg'); ?>
					</figure>
				</div>
			</div>
		</div>
	</section>
	<section class="About-mission">
		<div class="container">
			<div class="row">
				<div class="left col-md-6">
					<h3>Misión</h3>
					<div class="text">
						<p>Somos una Empresa dedicada a la producción, comercialización y exportación de productos no tradicionales, apegándonos a estándares internacionales, tales como: Buenas Prácticas Agrícolas (BPA), Buenas Prácticas de Manufactura (BPM), Manejo Integrados de Plagas (MIP), Análisis de Riesgos y Puntos Críticos de Control (APCC), Manejo Integrado de Plagas (MIP) y la Agencia para la Protección del Medio Ambiente (EPA) garantizando a la vez el respeto al medio ambiente y un producto de excelente calidad a nuestros clientes.</p>
					</div>
				</div>
				<div class="left col-md-6">
					<h3 class="vision">Visión</h3>
					<div class="text">
						<ul>
							<li>Consolidar un portafolio de productos de alta rentabilidad y del mas bajo riesgo agrícola.</li>
							<li>Enfocarnos en dirigir esfuerzos para ser competitivos en el mercado internacional. </li>
							<li>Ser los número uno en implementación de nuevas tecnologías en cultivos no tradicionales, manteniendo siempre 2 o 3 cultivos de alto potencial en investigación y desarrollo en pruebas de tamaño comercial para que puedan servir de proyectos pilotos.</li>
						</ul>
					</div>
				</div>

				<div class="left col-md-12">
					<h3>Nuestro Personal</h3>
					<div class="row">
						<div class="left col-md-12">
							<div class="text">
								<p>
									AGROESNICA, S. A., es una oportunidad laboral para profesionales, técnicos, y fuerza laboral agrícola tradicional bien experimentada ya que en todos ellos incurre la dinámica productiva y la solidez de la empresa, basándose en la investigación de nuevos cultivos no tradicionales, potenciales para el mercado internacional. 
								</p>
								<p>
									Con una misión clara, nuestra empresa se proyecta con su personal a continuar el desarrollo y sostenimiento de sus productos cumpliendo con los estándares internacionales, para lo cual, tiene puesta su confianza en su recurso humano, competitivo, capacitado e investigativo.
								</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="About-bottom">
		<?= $this->Html->image('/files/quienes-fotoGrupal.jpg'); ?>
	</section>
</div>