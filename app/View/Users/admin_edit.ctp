<div>
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="btn-group" role="group" aria-label="...">
				<?php echo $this->Html->link(__('<i class="fa fa-list"></i>&nbsp;&nbsp;Listar Usuarios'), array('action' => 'index'),array('class' => 'btn btn-primary', 'escape'=>false)); ?>
				<?php echo $this->Form->postLink(__('<i class="fa fa-times"></i>&nbsp;&nbsp;Eliminar Usuario'), array('action' => 'delete', $this->Form->value('User.id')), array('class' => 'btn btn-danger', 'escape'=>false,'confirm' => __('Seguro que desea eliminar el usuario %s?', $this->Form->value('User.username')))); ?>
			</div>
		</div>
	</div>
	<div class="users form">
	<?php echo $this->Form->create('User',array('inputDefaults' => array('required' => false))); ?>
		<fieldset>
			<legend><?php echo __('Editar Usuario'); ?></legend>
			<div class="row">
				<?php
				echo $this->Form->input('id');
				echo $this->Form->input('username', array('class'=>'form-control', 'div'=>array('class' => 'col-md-6'), 'label'=>'Usuario'));
				echo $this->Form->input('password', array('class'=>'form-control', 'div'=>array('class' => 'col-md-6'), 'label'=>'Contraseña')); ?>
			</div>
		</fieldset>
	<?php echo $this->Form->end(array('label'=>__('Guardar'),'class'=>'btn btn-success')); ?>
	</div>
</div>
