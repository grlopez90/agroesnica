<!DOCTYPE html>
<html>
  <head>
  	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Bootstrap -->
    <?php

		//echo $this->Html->css('http://necolas.github.io/normalize.css/2.1.1/normalize.css');
        //echo $this->Html->css('cake.generic');
        echo $this->Html->css('admin');

		echo $this->Html->script('http://code.jquery.com/jquery-2.0.3.min.js');
		//echo $this->Html->script('admin');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

	?>
  </head>
  <body>
    <?php //echo $this->Session->flash(); ?>
    <div class="row">
        <header class="container-fluid">
            <div id="brandLogin">
                <div class="wrapperLogo">
                    <?php echo $this->Html->image('admin/agroesNicaAdmin.png',array('height' => '100%')); ?>
                </div>
            </div>
        </header>
        <div class="container-fluid">
            <div class="row" style="margin:0;">
                <div id="wrapperLogin">
                    <?php
                    echo $this->Session->flash();
                    echo $this->Session->flash('auth', array('params' => array('class' => 'alert alert-danger')));
                    ?>
                    <div id="loginForm" class="wraperOut">
                        <?php
                        echo $this->Form->create(
                            'User',
                            array(
                                'inputDefaults' => array(
                                    'div' => false
                                )
                            )
                        );
                        echo $this->Form->input('username', array('label'=>'Usuario','class'=>'input form-control'));
                        echo $this->Form->input('password', array('label'=>'Contraseña','class'=>'input form-control'));

                        echo $this->Form->end(
                            array(
                                'label' => 'Iniciar Sesión',
                                'class' => 'btn btn-success'
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
  </body>
</html>
