<?php
foreach ($dtResults as $result) {
    $actions = $this->Html->link(__('<i class="fa fa-fw fa-pencil"></i> Editar'), array('action' => 'edit', $result['User']['id']), array('class'=>'btn btn-success', 'escape'=>false));
    $actions .= ' '.$this->Form->postLink(__('<i class="fa fa-fw fa-times"></i> Eliminar'), array('action' => 'delete', $result['User']['id']), array('class'=>'btn btn-danger', 'escape'=>false), __('Seguro que desea eliminar el usuario: %s?', $result['User']['username']));
    $this->dtResponse['aaData'][] = array(
        $result['User']['id'],
        $result['User']['username'],
        $result['User']['created'],
        $result['User']['modified'],
        $actions
    );
}
