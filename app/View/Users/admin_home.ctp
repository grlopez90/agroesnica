<ul class="bs-glyphicons">
	<li class="col-md-3 col-sm-4">
		<div class="headWrappers">
			<div class="glyphiconWrapper">
				<span class="fa fa-newspaper-o"></span>
			</div>
			<div class="titleWrapper">
				<span>Noticias</span>
			</div>
		</div>
		<div class="linksWrapper">
			<a href="<?php echo $this->Html->url('/admin/news'); ?>" class="btn btn-default"><i class="fa fa-list"></i>&nbsp;&nbsp;Ver Noticias</a>
    		<a href="<?php echo $this->Html->url('/admin/news/add'); ?>" class="btn btn-default"><i class="fa fa-plus"></i>&nbsp;&nbsp;Nueva Noticia</a>
		</div>
	</li>
	<li class="col-md-3 col-sm-4">
		<div class="headWrappers">
			<div class="glyphiconWrapper">
				<span class="fa fa-picture-o"></span>
			</div>
			<div class="titleWrapper">
				<span>Galerías</span>
			</div>
		</div>
		<div class="linksWrapper">
			<a href="<?php echo $this->Html->url('/admin/albums'); ?>" class="btn btn-default"><i class="fa fa-list"></i>&nbsp;&nbsp;Ver Galerías</a>
    		<a href="<?php echo $this->Html->url('/admin/albums/add'); ?>" class="btn btn-default"><i class="fa fa-plus"></i>&nbsp;&nbsp;Nueva Galería</a>
		</div>
	</li>
	<li class="col-md-3 col-sm-4">
		<div class="headWrappers">
			<div class="glyphiconWrapper">
				<span class="fa fa-user"></span>
			</div>
			<div class="titleWrapper">
				<span>Usuarios</span>
			</div>
		</div>
		<div class="linksWrapper">
			<a href="<?php echo $this->Html->url('/admin/users'); ?>" class="btn btn-default"><i class="fa fa-list"></i>&nbsp;&nbsp;Ver Usuarios</a>
    		<a href="<?php echo $this->Html->url('/admin/users/add'); ?>" class="btn btn-default"><i class="fa fa-plus"></i>&nbsp;&nbsp;Nuevo Usuario</a>
		</div>
	</li>
</ul>
