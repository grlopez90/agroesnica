<?php
App::uses('AppController', 'Controller');
/**
 * Albums Controller
 *
 * @property Album $Album
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class AlbumsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array(
		 'Funciones',
		 'Paginator',
		 'Flash',
		 'Session',
		 'DataTable.DataTable' => array(
			 'Album' => array(
				 'columns' => array(
					 'id' => 'Id',
					 'title' => 'Título',
					 'state' => 'Estado',
					 'created' => 'Creado',
					 'Acciones' => null
				 )
			 ),
			 'order' => array('id'=>'desc'),
			 'autoRender' => false,
			 'autoData' => false
		 )
	 );

	 public $helpers = array('DataTable.DataTable');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->DataTable->setViewVar(array('Album'));
		$this->set('title_for_layout', 'Galerías');
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Album->exists($id)) {
			throw new NotFoundException(__('Invalid album'));
		}
		$options = array('conditions' => array('Album.' . $this->Album->primaryKey => $id));
		$this->set('album', $this->Album->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->request->data['Album']['permalink'] = $this->Funciones->generatePermalink($this->request->data['Album']['title']);
			$this->request->data['Album']['user_id'] = $this->Auth->user('id');
			$this->Album->create();
			if ($this->Album->save($this->request->data)) {
				$this->Session->setFlash(__('La galería ha sido creada.'), 'default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La galería no fue creada. Por favor, intente nuevamente.'), 'default',array('class'=>'alert alert-danger'));
			}
		}
		$this->set('title_for_layout', 'Galerías');
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Album->exists($id)) {
			throw new NotFoundException(__('La galería no existe'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Album']['permalink'] = $this->Funciones->generatePermalink($this->request->data['Album']['title']);
			$this->request->data['Album']['user_id'] = $this->Auth->user('id');
			if ($this->Album->save($this->request->data)) {
				$this->Session->setFlash(__('La galería ha sido actualizada.'), 'default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La galería no fue actualizada. Por favor, intente nuevamente.'), 'default',array('class'=>'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Album.' . $this->Album->primaryKey => $id));
			$this->request->data = $this->Album->find('first', $options);
		}
		$this->set('title_for_layout', 'Galerías');
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Album->id = $id;
		if (!$this->Album->exists()) {
			throw new NotFoundException(__('La galería no existe'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Album->delete()) {
			$this->Session->setFlash(__('La galería ha sido eliminada.'), 'default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash(__('La galería no fue eliminada. Por favor, intente nuevamente.'), 'default',array('class'=>'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function index() {
		$albums = $this->Album->find('all',['conditions'=>['Album.state'=>1],'order'=>'Album.id DESC']);
		$this->set(compact('albums'));
	}

	public function view($id = null) {
		if (!$this->Album->exists($id)) {
			throw new NotFoundException(__('Invalid album'));
		}
		$options = array('conditions' => array('Album.' . $this->Album->primaryKey => $id));
		$this->set('album', $this->Album->find('first', $options));
	}

	public function beforeFilter() {
		parent::beforeFilter();

		if ($this->params['admin'] == 1)
			$this->layout = 'admin';
		else
			$this->layout = 'default';

		$this->Auth->allow('index', 'view'); // Letting users register themselves
	}
}
