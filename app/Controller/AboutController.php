<?php
App::uses('AppController', 'Controller');
/**
* About Controller
 *
 * @property About $About
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class AboutController extends AppController {

	public function index() {
		
	}

	public function beforeFilter() {
		parent::beforeFilter();

		if ($this->params['admin'] == 1)
			$this->layout = 'admin';
		else
			$this->layout = 'default';

		$this->Auth->allow('index'); // Letting users register themselves
	}
}