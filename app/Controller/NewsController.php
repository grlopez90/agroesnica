<?php
App::uses('AppController', 'Controller');
/**
 * News Controller
 *
 * @property News $News
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class NewsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array(
		 'Funciones',
		 'Paginator',
		 'Flash',
		 'Session',
		 'DataTable.DataTable' => array(
			 'News' => array(
				 'columns' => array(
					 'News.id' => 'Id',
					 'News.title' => 'Título',
					 'News.state' => 'Estado',
					 'News.created' => 'Creado',
					 'Acciones' => null
				 )
			 ),
			 'order' => array('id'=>'desc'),
			 'autoRender' => false,
			 'autoData' => false
		 )
	 );

	 public $helpers = array('DataTable.DataTable');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->DataTable->setViewVar(array('News'));
		$this->set('title_for_layout', 'Noticias');
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->News->exists($id)) {
			throw new NotFoundException(__('Invalid news'));
		}
		$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
		$this->set('news', $this->News->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->request->data['News']['permalink'] = $this->Funciones->generatePermalink($this->request->data['News']['title']);
			$this->request->data['News']['user_id'] = $this->Auth->user('id');
			$this->News->create();
			if ($this->News->save($this->request->data)) {
				$this->Session->setFlash(__('La noticia ha sido creada.'), 'default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'edit/'.$this->News->getLastInsertId()));
			} else {
				$this->Session->setFlash(__('La noticia no fue creada. Por favor, intente nuevamente.'), 'default',array('class'=>'alert alert-danger'));
			}
		}
		$this->set('title_for_layout', 'Noticias');
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->News->exists($id)) {
			throw new NotFoundException(__('La noticia no existe'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['News']['permalink'] = $this->Funciones->generatePermalink($this->request->data['News']['title']);
			$this->request->data['News']['user_id'] = $this->Auth->user('id');
			if ($this->News->save($this->request->data)) {
				$this->Session->setFlash(__('La noticia ha sido actualizada.'), 'default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La noticia no fue actualizada. Por favor, intente nuevamente.'), 'default',array('class'=>'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('News.' . $this->News->primaryKey => $id));
			$this->request->data = $this->News->find('first', $options);
		}
		$this->set('title_for_layout', 'Noticias');
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->News->id = $id;
		if (!$this->News->exists()) {
			throw new NotFoundException(__('La noticia no existe'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->News->delete()) {
			$this->Session->setFlash(__('La noticia ha sido eliminada.'), 'default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash(__('La noticia no fue eliminada. Por favor, intente nuevamente.'), 'default',array('class'=>'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function index() {
		
	}

	public function view($id = null) {
		
	}

	public function beforeFilter() {
		parent::beforeFilter();

		if ($this->params['admin'] == 1)
			$this->layout = 'admin';
		else
			$this->layout = 'default';

		$this->Auth->allow('index', 'view'); // Letting users register themselves
	}
}
