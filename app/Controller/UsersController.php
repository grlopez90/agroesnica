<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array(
		'Paginator',
		'Flash',
		'Session',
		'DataTable.DataTable' => array(
			'User' => array(
				'columns' => array(
					'id' => 'Id',
					'username' => 'Nombre de Usuario',
					'created' => 'Creado',
					'modified' => 'Modificado',
					'Acciones' => null
				)
			),
			'order' => array('id'=>'desc'),
			'autoRender' => false,
			'autoData' => false
		)
	);

    public $helpers = array('DataTable.DataTable');

/**
 * admin_index method
 *
 * @return void
 */
	 public function admin_index() {
	 	$this->DataTable->setViewVar(array('User'));
	 }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if (!empty($this->request->data['User']['password']))
				$this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('El usuario ha sido creado.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El usuario no fue creado. Por favor, intente nuevamente.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('El usuario no existe'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$reg = $this->User->find(
				'first',
				array(
					'conditions' => array(
						'User.id' => $id
					)
				)
			);
			if ($this->request->data['User']['password'] != $reg['User']['password'])
				$this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('El usuario ha sido actualizado.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El usuario no fue actualizado. Por favor, intente nuevamente.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('El usuario no existe'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('El usuario ha sido eliminado.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('El usuario no fue eliminado. Por favor, intente nuevamente.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_home() {
		$this->set(array('title_for_layout' => 'Administrador de Contenidos AgroesNica'));
	}

	public function admin_login() {
		$this->layout = false;
		if ($this->Auth->User()){
			$this->redirect('/admin/users/home');
		}
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Usuario o Contraseña Inválidos'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$this->set('title_for_layout', 'Iniciar Sesión');
	}

	public function admin_logout() {
		$this->redirect($this->Auth->logout());
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('title_for_layout', 'Usuarios');
		$this->layout = 'admin';
	}
}
