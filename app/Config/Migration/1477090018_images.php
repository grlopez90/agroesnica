<?php
class Images extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'images';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'images' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
					'seccion' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 50),
					'seccion_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
					'extension' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 5),
					'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
					'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
				)
			)
		),
		'down' => array(
			'drop_table' => array('images')
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		return true;
	}
}
