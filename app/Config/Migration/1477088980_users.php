<?php
class Users extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'users';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'users' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
					'username' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 255),
					'password' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 255),
					'created' => array('type' => 'timestamp', 'null' => false, 'default' => NULL),
					'modified' => array('type' => 'timestamp', 'null' => false, 'default' => NULL),
					'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
					'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
				)
			)
		),
		'down' => array(
			'drop_table' => array('users')
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		$User = ClassRegistry::init('User');
	    if ($direction === 'up') {
	        $data[0]['User']['username'] = 'admin';
			$data[0]['User']['password'] = sha1('a659c113b69fe2109080bbc0283ac342e373c5f9admin');

	        $User->create();
	        if ($User->saveAll($data)) {
	            $this->callback->out('Los registros de la tabla users han sido creados');
	        }
	    } elseif ($direction === 'down') {
	        // do more work here
	    }
		return true;
	}
}
