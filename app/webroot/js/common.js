$(".group_photo").colorbox({
	rel:'group_photo', 
	transition:"elastic",
	maxWidth:"80%", 
	maxHeight:"80%",
	fixed: true,
	current: "Foto {current} de {total}"
});

$(".youtube").colorbox({
	iframe:true,
	innerWidth:720, 
	innerHeight:405}
);