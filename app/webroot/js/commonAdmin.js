; (function ($, window, undefined) {

    $.fn.savePrize = function(table) {
        $('#PrizeAdminForm').submit(function(event){
            event.preventDefault();
            var url = $(this).attr('action');
            var tbody = $(table).children('tbody');
            $.ajax({
				type	: "POST",
				url		: url,
                data    : $(this).serialize(),
				async	: false,
				success	: function(data){
                    $(data).appendTo(tbody);
                    //console.log(this);
				}
			});
        });
    }

    $.fn.deletePrize = function() {
		$('.deletePrize').click(function(event){

			event.preventDefault();

			var url = $(this).attr('href');
			var conten = $(this).attr('data');

			$.ajax({
				type	: "POST",
				url		: url,
				async	: false,
				success	: function(){
					//location.reload();
					console.log('#'+conten);
					$('#'+conten).fadeOut(500, function() {
						$(this).remove()
					});
				}
			});
		});
	}

    $.fn.saveWinner = function(table) {
        $('#WinnerAdminForm').submit(function(event){
            event.preventDefault();
            var url = $(this).attr('action');
            var content = $('#winnersWrap');
            $.ajax({
				type	: "POST",
				url		: url,
                data    : $(this).serialize(),
				async	: false,
				success	: function(data){
                    $(data).prependTo(content);
                    //console.log(this);
				}
			});
        });
    }

    $.fn.deleteWinner = function() {
		$('.deleteWinner').click(function(event){

			event.preventDefault();

			var url = $(this).attr('href');
			var conten = $(this).attr('data');

			$.ajax({
				type	: "POST",
				url		: url,
				async	: false,
				success	: function(){
					//location.reload();
					console.log('#'+conten);
					$('#'+conten).fadeOut(500, function() {
						$(this).remove()
					});
				}
			});
		});
	}

    $.fn.delete_img = function() {
		$('.drop').click(function(event){

			event.preventDefault();

			var url = $(this).attr('href');
			var conten = $(this).attr('data');

			$.ajax({
				type	: "POST",
				url		: url,
				async	: false,
				success	: function(){
					//location.reload();
					console.log('#'+conten);
					$('#'+conten).fadeOut(500, function() {
						$(this).remove()
					});
				}
			});
		});
	}

    $.fn.prizesConvert = function() {
        return this.each(function(){
            var string;
            var stringDiv;
            $tarea = $(this);
            stringDiv = $tarea.text().split(' ');
            for (var i = 0; i <= stringDiv.length - 1; i++) {
                stringDiv[i] = $.trim(stringDiv[i]);

                switch (stringDiv[i]) {
                    case '1':
                        string = $(this).text().replace(stringDiv[i],"Primer Premio");
                        $(this).text(string);
                        break;
                    case '2':
                        string = $(this).text().replace(stringDiv[i],"Segundo Premio");
                        $(this).text(string);
                        break;
                    case '3':
                        string = $(this).text().replace(stringDiv[i],"Tercer Premio");
                        $(this).text(string);
                        break;
                    case '4':
                        string = $(this).text().replace(stringDiv[i],"Cuarto Premio");
                        $(this).text(string);
                        break;
                    case '5':
                        string = $(this).text().replace(stringDiv[i],"Quinto Premio");
                        $(this).text(string);
                        break;
                    case '6':
                        string = $(this).text().replace(stringDiv[i],"Sexo Premio");
                        $(this).text(string);
                        break;
                }
                string = "";
            }
            i++;
        });
    }

}) (jQuery, window);

$(function(){
	tinymce.init({
      	relative_urls: false,
      	selector: "textarea.wysiwyg",
      	plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor filemanager"
            ],
            image_advtab: true,
      	toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | image | preview code",
      	height : 300,
      	resize : false
  	});
});
